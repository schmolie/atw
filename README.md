This project was built for educational purposes as I wanted to learn more JavaScript and React and is still under development.

I wanted to create a web app for bookmarking and decided to use Contentful a headless CMS, React, Redux and Webpack.

1. Run "npm install"
2. Run "npm run dev"
3. Visit localhost:9000
