const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const path = require("path");
const config = require('./config.json');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
    entry: {
        assets: [
            "./app/scripts/scripts.js",
            "./app/styles/styles.scss"
        ]
    },
    output: {
        path: path.resolve('./app'),
        filename: 'scripts.bundle.js'
    },
    devServer: {
        historyApiFallback: true,
        contentBase: './app',
        compress: true,
        port: 9000,
        open: true
    },
    devtool: 'source-map',
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ['css-loader', 'postcss-loader?sourceMap=inline', 'sass-loader'],
                    publicPath: './app'
                })
            },
            {
                test: /\.jsx|\.js?$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                    presets: ['react', 'es2015', 'stage-0'],
                    plugins: ["transform-decorators-legacy", "transform-class-properties"]
                }
            },
        ]
   },
    plugins: [
        new ExtractTextPlugin({
            filename: 'styles.bundle.css',
            disable: false,
            allChunks: true
        }),
        new UglifyJSPlugin({
            output: {
                comments: false
            },
            mangle: true,
            sourcemap: true,
            debug: false,
            minimize: true,
            compress: {
                warnings: false,
                screw_ie8: true,
                conditionals: true,
                unused: true,
                comparisons: true,
                sequences: true,
                dead_code: true,
                evaluate: true,
                if_return: true,
                join_vars: true
            }
        }),
        new webpack.DefinePlugin({
            'process.env': {
                'NODE_ENV': JSON.stringify('production'),
            }
        })
    ]
};
