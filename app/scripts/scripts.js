import React, {Component} from "react"
import {render} from "react-dom"
import { BrowserRouter as Router, Route, IndexRoute, Link} from 'react-router-dom'

import { Provider } from "react-redux"
import store from "./store"

import Main from "./components/main/Main.js"
import PostSingle from "./components/post-single/PostSingle.js"

render((
    <Provider store={ store }>
        <Router>
            <div>
                <Route exact path="/" component={Main} />
                <Route path="/post/:slug" component={PostSingle} />
            </div>
        </Router>
    </Provider>
    ), document.getElementById('app')
)
