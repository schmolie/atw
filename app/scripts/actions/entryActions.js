import { client } from '../service/client'
import { space } from '../../../config.json'
import { clientManagement } from '../service/client'
import { contentTypeFields } from '../service/contentTypeFields'

/**
 * Fetch Entries from Contentful and notify entry reducer
 */
export function fetchEntries() {
    // same as return function(dispatch)
    return dispatch => {

        const args = {
            'order': '-sys.updatedAt'
        }

        client.getEntries( args )
        .then((response) => {
            dispatch({ type: 'FETCH_ENTRIES_FULFILLED', payload: response.items })
        })
        .catch((err) => {
            dispatch({ type: 'FETCH_ENTRIES_REJECTED', payload: err })
        })
    }
}

/**
 * Create Entry and notify create reducer
 */
export function createEntry(create, entry = null) {
    if (entry != null) {
        return dispatch => {
            clientManagement.getSpace(space)
            .then((space) => {
                space.createEntry(entry.contentTypeId, contentTypeFields(entry))
                .then((entry) => {
                    entry.publish()
                    dispatch({ type: 'CREATE_ENTRY_FULFILLED', payload: entry })
                    dispatch({ type: 'NOTIFICATION', payload: 'Entry Published' })
                })
                .catch((err) => {
                    dispatch({ type: 'CREATE_ENTRY_REJECTED', payload: err })
                    dispatch({ type: 'NOTIFICATION', payload: 'Something went wrong, try agian!' })
                })
            })
        }
    } else {
        return {
            type: 'CREATE_ENTRY_PENDING',
            payload: create
        }
    }
}

/**
 * Edit entry and notify edit reducer
 */
export function editEntry(entryId, entry, contentType) {
    return dispatch => {
        dispatch({ type: 'EDIT_ENTRY_ID', payload: entryId })
        dispatch({ type: 'EDIT_ENTRY_CONTENT_TYPE', payload: contentType })
        dispatch({ type: 'EDIT_ENTRY_PENDING', payload: entry })
    }
}

/**
 * Publish edited entry and notify edit reducer
 */
export function editEntryPublish(entryId, fields) {
    return dispatch => {
        clientManagement.getSpace(space)
        .then((space) => {
            space.getEntry(entryId)
            .then((entry) => {
                entry.fields.title = { 'en-US': fields.title }
                entry.fields.tags = { 'en-US': fields.tags }

                if (fields.slug) entry.fields.slug = { 'en-US': fields.slug }
                if (fields.body) entry.fields.body = { 'en-US': fields.body }
                if (fields.url) entry.fields.url = { 'en-US': fields.url }
                if (fields.iframe) entry.fields.iframe = { 'en-US': fields.iframe }

                return entry.update()
            })
            .then((entry) => {
                entry.publish()
                dispatch({ type: 'EDIT_ENTRY_FULFILLED', payload: entry })
                dispatch({ type: 'NOTIFICATION', payload: 'Entry Updated' })
            })
            .catch((err) => {
                // promise middleware type EDIT_ENTRY_REJECTED
                dispatch({ type: 'EDIT_ENTRY_REJECTED', payload: err })
                dispatch({ type: 'NOTIFICATION', payload: 'Something went wrong, try agian!' })
           })
        })
    }
}

/**
 * Delete entry and notify delete reducer
 */
export function deleteEntry(entryID) {
    return dispatch => {
        clientManagement.getSpace(space)
        .then((space) => {
            dispatch({ type: 'DELETE_ENTRY', payload: space.getEntry(entryID)
                .then((entry) => entry.unpublish())
                .then((entry) => {
                    entry.delete()
                    dispatch({ type: 'NOTIFICATION', payload: 'Entry deleted' })
                })
            })
        })
        .catch((err) => {
            dispatch({ type: 'DELETE_ENTRY', payload: err })
        })
    }
}
