export function searchQuery(query, entries) {
    return dispatch => {
        dispatch({ type: 'SEARCH_QUERY', payload: query })
        dispatch({ type: 'SEARCH_RESULT', payload: entries })
    }
}
