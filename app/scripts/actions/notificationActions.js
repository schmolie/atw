export function dismissNotification(payload) {
    return dispatch => {
        dispatch({ type: 'DISMISS_NOTIFICATION', payload: payload })
    }
}
