export function contentTypeFields(data) {

    let entry = {
        fields: {
            title: { 'en-US': data.title },
            tags: { 'en-US': data.tags }
        }
    }

    switch (data.contentTypeId) {
        case 'post':
            entry.fields.body = { 'en-US': data.body }
            entry.fields.slug = { 'en-US': data.slug }
        return entry
        case 'url':
        case 'video':
            entry.fields.url = { 'en-US': data.url }
        return entry
    }

    return entry
}

export function setEntryFields(entry = {}) {

    let fields = {
        title: (entry.title) ? entry.title : '',
        slug: (entry.slug) ? entry.slug : '',
        body: (entry.body) ? entry.body : '',
        url: (entry.url) ? entry.url : '',
        tags: (entry.tags) ? entry.tags : [],
        tag: ''
    }

    return fields
}
