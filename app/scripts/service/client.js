import { createClient } from 'contentful'
import { accessToken, accessTokenManagement, space } from '../../../config.js'

const contentful = require('contentful-management')

export const client = createClient({
    space: space,
    accessToken: accessToken
})

export const clientManagement = contentful.createClient({
    accessToken: accessTokenManagement
})
