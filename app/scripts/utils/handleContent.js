export function contentStripHtmlTags(content) {
    return content.replace(/<(?:.|\n)*?>/gm, '');
}

export function excerptStripHtmlTags(content) {
    content = contentStripHtmlTags(content)

    return content.replace(/(([^\s]+\s\s*){20})(.*)/,"$1…")
}
