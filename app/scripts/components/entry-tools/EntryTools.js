import React from 'react'
import { editEntry, deleteEntry } from "../../actions/entryActions"
import { connect } from "react-redux"
import store from "../../store"

import { TiPencil, TiHeartOutline } from  'react-icons/lib/ti'
import { FaTrashO } from  'react-icons/lib/fa'
import { MdMoreVert } from  'react-icons/lib/md'

import Tags from '../tags/Tags'

@connect((store) => {
    return {
        edit: store.edit
    }
})

export default class EntryTools extends React.Component  {

    constructor(props) {
        super(props)

        this.state = {
            toggleTools: false
        }

        this.edit = this.edit.bind(this)
        this.delete = this.delete.bind(this)
        this.renderTags = this.renderTags.bind(this)
        this.toggleTools = this.toggleTools.bind(this)
        this.handleOutsideClick = this.handleOutsideClick.bind(this)
    }

    edit() {
        return this.props.dispatch(
            editEntry(this.props.entryId, this.props.fields, this.props.contentType)
        )
    }

    delete() {
        return this.props.dispatch(
            deleteEntry(this.props.entryId)
        )
    }

    toggleTools() {
        // attach/remove event handler
        if (!this.state.toggleTools) {
          document.addEventListener('click', this.handleOutsideClick, false);
        } else {
          document.removeEventListener('click', this.handleOutsideClick, false);
        }

        this.setState({
            toggleTools: !this.state.toggleTools
        })
    }

    handleOutsideClick(e) {
        // ignore clicks on the component itself
        if (this.node.contains(e.target)) {
          return;
        }

        this.toggleTools();
      }

    renderTags(e) {
        if (this.props.fields.tags ) return <Tags filter={true} tags={ this.props.fields.tags } />
    }

    render() {

        let toggleTools = (this.state.toggleTools) ? ' toggle-tools' : ''

        return (
            <div className={ 'entry-tools' + toggleTools } ref={ node => { this.node = node } }>

                { toggleTools ? (
                    <ul className="entry-tools--tools">
                        <li className="entry-tools--delete" onClick={ this.delete }>
                            <FaTrashO/>
                        </li>
                        <li className="entry-tools--edit" onClick={ this.edit }>
                            <TiPencil/>
                        </li>
                        <li className="entry-tools--add-favourite">
                            <TiHeartOutline/>
                        </li>
                    </ul>
                ) : null }

                { this.renderTags() }

                <span className="entry-tools--toggle" onClick={ this.toggleTools }>
                    <MdMoreVert/>
                </span>
            </div>
        )
    }
}
