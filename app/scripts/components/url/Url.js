import React, {PropTypes} from 'react'
import EntryTools from '../entry-tools/EntryTools'

export default class Url extends React.Component  {

    render() {
        let url = this.props.entry

        return (
            <article className="entry url" data-id={ url.sys.id }>
                <h2>
                    <a href={url.fields.url} target="_blank">{url.fields.title}</a>
                </h2>
                <EntryTools fields={ url.fields } entryId={ url.sys.id } contentType={ url.sys.contentType.sys.id } />
            </article>
        )
    }
}
