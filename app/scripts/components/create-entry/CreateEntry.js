import React from 'react'
import { createEntry } from "../../actions/entryActions"

import { TiTimes, TiDocument } from  'react-icons/lib/ti'
import { MdAddCircleOutline, MdPlayArrow, MdLink } from  'react-icons/lib/md'

import { connect } from "react-redux"
import store from "../../store"

import Tags from  '../tags/Tags'

@connect((store) => {
    return {
        create: store.create
    }
})
export default class CreateForm extends React.Component  {

    constructor(props) {
        super(props)

        // Set initial local state
        this.state = {
            contentTypeId: "post",
            title: '',
            slug: '',
            body: '',
            tags: [],
            tag: '',
            url: ''
        }

        this.handleInputChange = this.handleInputChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.close = this.close.bind(this)
        this.addTag = this.addTag.bind(this)
        this.removeTag = this.removeTag.bind(this)
        this.renderTags = this.renderTags.bind(this)

    }

    componentDidMount() {
        document.body.classList.add('entry-form-active')
    }

    componentWillUnmount() {
        document.body.classList.remove('entry-form-active')
    }

    /**
     * Update local state based on user inputs
     */
    handleInputChange(e) {
        const name = e.target.name
        const value = e.target.value

        this.setState({
            [name]: value
        })
    }

    /**
     * Dispatch new entry with createEntry action
     */
    handleSubmit(e) {
        e.preventDefault();
        this.props.dispatch(
            createEntry(false, this.state)
        )
    }

    /**
     * Dispatch and set store.create.active to false
     */
    close(e) {
        this.props.dispatch(
            createEntry(false)
        )
    }

    /**
     * Add tag to tags array if tag doesn't exists and update local state
     */
    addTag(e) {
        let tag = this.state.tag
        let tags = this.state.tags

        if ( tags.indexOf( tag ) == -1 && tag.length > 0 ) {
            this.setState({
                tags: this.state.tags.concat(this.state.tag),
                tag: ''
            })
        } else {
            console.log('tag already exists');
        }
    }


    /**
     * Remove tag if tag exists and update local state
     */
    removeTag(e) {
        let index = this.state.tags.indexOf(e.target.getAttribute('data-tag'))
        this.setState({
            tags: this.state.tags.filter((_, i) => i !== index)
        });
    }

    /**
     * Render if tags array has at least one item
     */
    renderTags() {
        if (this.state.tags.length > 0) {
            return <Tags onClickHandle={this.removeTag} tags={this.state.tags} />
        }
    }

    render() {

        let contentType = this.state.contentTypeId

        return (
            <div className="entry-form">
                <form className={'create-entry ' + this.state.contentTypeId} onSubmit={this.handleSubmit}>
                    <h2>Add Entry</h2>
                    <span className="entry-form--close" onClick={ this.close }>
                        <TiTimes />
                    </span>
                    <div className="entry-form--type field">
                            <input id="post" type="radio" name="contentTypeId" value="post" onChange={this.handleInputChange} defaultChecked/>
                            <label htmlFor="post">
                                <TiDocument/>
                                <span>Post</span>
                            </label>
                            <input id="url" type="radio" name="contentTypeId" value="url" onChange={this.handleInputChange} />
                            <label htmlFor="url">
                                <MdLink/>
                                <span>Url</span>
                            </label>
                            <input id="video" type="radio" name="contentTypeId" value="video" onChange={this.handleInputChange} />
                            <label htmlFor="video">
                                <MdPlayArrow/>
                                <span>Video</span>
                            </label>
                    </div>
                    <div className="entry-form--title field">
                        <label htmlFor="title">Title</label>
                        <input id="title" type="text" name="title" value={this.state.title} onChange={this.handleInputChange} autocomplete="off" required />
                    </div>

                    { contentType == 'post' ? (
                        <div className="entry-form--slug field">
                            <label htmlFor="slug">Slug</label>
                            <input id="slug" type="text" name="slug" value={ this.state.slug } onChange={ this.handleInputChange } autoComplete="off" required />
                        </div>
                    ) : null }

                    <div className="entry-form--tags field">
                        <label htmlFor="tags">Tags</label>
                        <span className="entry-form--add-tag" onClick={ this.addTag }><MdAddCircleOutline/></span>
                        <input id="tag" type="text" name="tag" value={ this.state.tag } onChange={this.handleInputChange} autocomplete="off"/>
                    </div>

                    { this.renderTags() }

                    { contentType == 'url' || contentType == 'video' ? (
                        <div className="entry-form--url field">
                            <label htmlFor="url">Url</label>
                            <input id="url" type="text" name="url" value={ this.state.url } onChange={ this.handleInputChange } autoComplete="off" required />
                        </div>
                    ) : null }


                    { contentType == 'post' ? (
                        <div className="entry-form--body field">
                            <label htmlFor="body">Body</label>
                            <textarea id="body" type="text" name="body" value={ this.state.body } onChange={this.handleInputChange} required />
                        </div>
                    ) : null }

                    <input className="entry-form--submit" type="submit" name="submit" />
                </form>
            </div>
        )
    }
}
