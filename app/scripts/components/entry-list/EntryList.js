import React from 'react'
import Post from '../post/Post'
import Video from '../video/Video'
import Url from '../url/Url'

export default class EntryList extends React.Component  {

    constructor(props) {
        super(props)
    }

    render() {

        let entriesArray = this.props.entries

        const entries = entriesArray.map((entry, i) => {
            switch (entry.sys.contentType.sys.id) {
                case 'post':
                    return <Post id={i} key={i} entry={entry} />
                case 'video':
                    return <Video id={i} key={i} entry={entry} />
                case 'url':
                    return <Url id={i} key={i} entry={entry} />
            }
        })

        return (
            <div className="entry-list">
                { entries }
            </div>
        )
    }
}
