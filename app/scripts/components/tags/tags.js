import React, {PropTypes} from 'react'

export default class Tags extends React.Component  {

    constructor(props) {
        super(props)
    }

    render() {

        let tags = this.props.tags.map((tag, i) => {
            return (
                <li key={'item-' + tag + '-' + i} className="entry--tag" data-tag={ tag } onClick={ this.props.onClickHandle } >{ tag }</li>
            )
        })

        return (
            <ul className="entry--tags" >
                { tags }
            </ul>
        )
    }
}
