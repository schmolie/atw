import React, {PropTypes} from 'react'
import { Link } from 'react-router-dom'
import EntryTools from '../entry-tools/EntryTools'

import { excerptStripHtmlTags } from '../../utils/handleContent'

export default class Post extends React.Component  {

    constructor(props) {
        super(props)

    }

    renderTags() {
        if (this.props.entry.fields.tags) return <Tags tags={ this.props.entry.fields.tags } />
    }

    render() {
        let post = this.props.entry
        return (
            <article className="entry post" data-id={ post.sys.id } >
                <header className="entry-header">
                    <h2>
                        <Link to={`/post/${post.fields.slug}`}>
                            { post.fields.title }
                        </Link>
                    </h2>
                    <p> { excerptStripHtmlTags(post.fields.body) } </p>
                </header>
                <EntryTools fields={ post.fields } entryId={ post.sys.id } contentType={ post.sys.contentType.sys.id } />
            </article>
        )
    }
}
