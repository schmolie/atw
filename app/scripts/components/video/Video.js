import React, {PropTypes} from 'react'
import EntryTools from '../entry-tools/EntryTools'

export default class Video extends React.Component  {

    constructor(props) {
        super()
    }

    render() {
        let video = this.props.entry

        return (
            <article className="entry video" data-id={ video.sys.id } >
                <h2><a href={ video.fields.url } target="_blank">{ video.fields.title }</a></h2>
                <EntryTools fields={ video.fields } entryId={ video.sys.id } contentType={ video.sys.contentType.sys.id } />
            </article>
        )
    }
}
