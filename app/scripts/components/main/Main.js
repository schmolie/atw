import React from "react"
import { render } from "react-dom"
import { connect } from "react-redux"
import { fetchEntries, createEntry } from "../../actions/entryActions"
import { dismissNotification } from "../../actions/notificationActions"
import store from "../../store"

import Header from  "../header/Header"
import Notification from  "../notification/Notification"
import EntryList from  "../entry-list/EntryList"
import EditEntry from  "../edit-entry/EditEntry"
import CreateEntry from  "../create-entry/CreateEntry"

@connect((store) => {
    return {
        entries: store.entry.entries,
        create: store.create,
        edit: store.edit,
        notification: store.notification,
        search: store.search
    }
})

export default class Main extends React.Component  {

    constructor(props) {
        super(props)

        this.entries = this.entries.bind(this)
        this.toggleCreate = this.toggleCreate.bind(this)
        this.toggleNotification = this.toggleNotification.bind(this)
        this.renderNotification = this.renderNotification.bind(this)
        this.renderEditEntry = this.renderEditEntry.bind(this)
        this.renderCreateEntry = this.renderCreateEntry.bind(this)
        this.renderEntryList = this.renderEntryList.bind(this)
    }

    /**
     * Fetch entries when component mounts
     */
    componentDidMount() {
        this.props.dispatch(fetchEntries())
    }

    /**
     * Toggle notification component if notification is active
     */
    toggleNotification(e) {
        e.preventDefault()

        if ( e.currentTarget.value == 'dismiss' ) {
            this.props.dispatch(
                dismissNotification(false)
            )
        }
    }

    /**
     * Dispatch and toggle store.create.active
     */
    toggleCreate(e) {
        e.preventDefault()

        this.props.dispatch(
            createEntry(!this.props.create.active)
        )
    }

     /**
     *  Return search results if there is one else return fetched entries
     */
    entries() {
        return this.props.search.entries.length > 0 ? this.props.search.entries : this.props.entries
    }

     /**
     * Render search results if there is one else render fetched entries
     */
    renderEntryList() {
        if ( !this.props.search.entries.length && this.props.search.query) return ( <h2 className="search-no-result">Oh no! Found no matches on { this.props.search.query }</h2> )
        return <EntryList entries= { this.entries() } />
    }

    /**
     * Render notification component if notification is active
     */
    renderNotification() {
        if (this.props.notification.active) return (
            <Notification toggleNotification={ this.toggleNotification } notification={ this.props.notification.message } />
        )
    }

    /**
     * Render EditEntry component if edit is active
     */
    renderEditEntry() {
        if (this.props.edit.active) return (
            <EditEntry fields={ this.props.edit.entry } />
        )
    }

    /**
     * Render CreateEntry component if create is active
     */
    renderCreateEntry() {
        if (this.props.create.active) return (
            <CreateEntry/>
        )
    }

    render() {

        const { create } = this.props

        return (
             <div>
                <Header toggleCreate={ this.toggleCreate } create={ create.active }/>
                { this.renderNotification() }
                <div className={'main-content'}>
                    { this.renderCreateEntry() }
                    { this.renderEditEntry() }
                    { this.renderEntryList() }
                </div>
            </div>
        )
    }
}
