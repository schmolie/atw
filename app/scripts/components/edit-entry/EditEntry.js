import React from 'react'
import { editEntry, editEntryPublish } from "../../actions/entryActions"

import { TiTimes } from  'react-icons/lib/ti'
import { MdAddCircleOutline } from  'react-icons/lib/md'

import { connect } from "react-redux"
import store from "../../store"
import Tags from  '../tags/Tags'

import { setEntryFields } from '../../service/contentTypeFields'

@connect((store) => {
    return {
        edit: store.edit
    }
})
export default class EditEntry extends React.Component  {

    constructor(props) {
        super(props)

        // Set initial local state
        this.state = setEntryFields(this.props.fields)

        this.handleInputChange = this.handleInputChange.bind(this)
        this.close = this.close.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
        this.addTag = this.addTag.bind(this)
        this.removeTag = this.removeTag.bind(this)
    }

    componentDidMount() {
        document.body.classList.add('entry-form-active')
    }

    componentWillUnmount() {
        document.body.classList.remove('entry-form-active')
    }

    /**
     * If the user made any changes update this.props.fields
     * and update local state
     */
    componentWillReceiveProps(nextProps) {
        if (nextProps.fields) this.setState(nextProps.fields)
    }

     /**
     * Update local state based on user inputs
     */
    handleInputChange(e) {
        const name = e.target.name
        const value = e.target.value

        this.setState({
            [name]: value
        })
    }

    /**
     * Dispatch and publish changes to entry
     */
    handleSubmit(e) {
        e.preventDefault();
        this.props.dispatch(
            editEntryPublish(this.props.edit.active, this.state)
        )
    }

    /**
     * Dispatch and set store.edit.active to false
     */
    close(e) {
        this.props.dispatch(editEntry(false, null))
    }

    /**
     * Add tag to tags array if tag doesn't exists and update local state
     */
    addTag(e) {
        let tag = this.state.tag
        let tags = this.state.tags

        if ( tags.indexOf( tag ) == -1 && tag.length > 0 ) {
            this.setState({
                tag: '',
                tags: tags.concat(tag)
            })
        }
    }

    /**
     * Remove tag if tag exists and update local state
     */
    removeTag(e) {
        // filter tags array and remove if targets data-tag exists in array
        let index = this.state.tags.indexOf(e.target.getAttribute('data-tag'))
        this.setState({ tags: this.state.tags.filter((_, i) => i !== index) });
    }

    render() {

        let contentType = this.props.edit.contentType

        return (
            <div className="entry-form">
                <form className={'edit-entry ' + this.props.edit.contentType } onSubmit={this.handleSubmit}>

                    <span className="entry-form--close" onClick={ this.close }>
                        <TiTimes />
                    </span>

                    <h2>Edit Entry</h2>

                    <div className="entry-form--title field">
                        <label htmlFor="title">Title</label>
                        <input id="title" type="text" name="title" value={ this.state.title } onChange={ this.handleInputChange } autoComplete="off" required />
                    </div>

                    { contentType == 'post' ? (
                        <div className="entry-form--slug field">
                            <label htmlFor="slug">Slug</label>
                            <input id="slug" type="text" name="slug" value={ this.state.slug } onChange={ this.handleInputChange } autoComplete="off" required />
                        </div>
                    ) : null }

                    <div className="entry-form--tags field">
                        <label htmlFor="tags">Tags</label>
                        <span className="entry-form--add-tag" onClick={ this.addTag }><MdAddCircleOutline/></span>
                        <input id="tag" type="text" name="tag" data-tag={ this.state.tag } value={ this.state.tag } onChange={this.handleInputChange} />
                    </div>

                    { this.state.tags ? (
                        <Tags onClickHandle={ this.removeTag } tags={ this.state.tags } />
                    ) : null }

                    { contentType == 'url' || contentType == 'video' ? (
                        <div className="entry-form--url field">
                            <label htmlFor="url">Url</label>
                            <input id="url" type="text" name="url" value={ this.state.url } onChange={ this.handleInputChange } autoComplete="off" required />
                        </div>
                    ) : null }


                    { contentType == 'post' ? (
                        <div className="entry-form--body field">
                            <label htmlFor="body">Body</label>
                            <textarea id="body" type="text" name="body" value={ this.state.body } onChange={this.handleInputChange} required />
                        </div>
                    ) : null }

                    <input className="entry-form--submit" type="submit" name="submit" />

                </form>
            </div>
        )
    }
}
