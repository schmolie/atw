import React from "react"
import { Link } from 'react-router-dom'
import { FaSearch } from  'react-icons/lib/fa'
import { TiHeartFullOutline, TiThList, TiDocumentAdd } from  'react-icons/lib/ti'
import Search from  '../search/Search'

export default class Header extends React.Component {

    constructor(props) {
        super(props)

    }

    render() {

        let create = (this.props.create) ? ' active' : ''

        return (
            <div className={ 'main-header' }>
                <header className="logo">
                    <h1><Link to="/">atw</Link></h1>
                </header>
                <Search/>
                <ul className="tools">
                    <li className="search-icon">
                        <FaSearch/>
                    </li>
                    <li className={'add' + create } onClick={ this.props.toggleCreate }>
                        <TiDocumentAdd/>
                    </li>
                    <li className="heart">
                        <TiHeartFullOutline/>
                    </li>
                </ul>
            </div>
        )
    }
}
