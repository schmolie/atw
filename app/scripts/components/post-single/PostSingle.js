import React, {Component} from 'react'
import marked from 'marked'
import { client } from '../../service/client'
import { contentStripHtmlTags } from '../../utils/handleContent'

export default class PostSingle extends React.Component {
    constructor(props) {
        super(props)

        // Move to Redux
        this.state = {
            post: {
                fields: {
                    title: '',
                    body: ''
                }
            }
        }
    }

    componentDidMount() {
        const params = this.props.match.params

        // Move to Redux
        if (params && params.slug) {
            client.getEntries({content_type: 'post', 'fields.slug': params.slug })
            .then((response) => {
                this.setState({post: response.items[0]})
            })
        }

    }

    getParsedMarkdown(content) {
        return {
            __html: marked(content, { sanitize: false })
        }
    }

    render() {
        return (
            <article className="entry post">
                <h2>{this.state.post.fields.title}</h2>
                <div dangerouslySetInnerHTML= {this.getParsedMarkdown(this.state.post.fields.body)} />
                <ul className="post--tags-list">
                </ul>
            </article>
        )
    }
}
