import React from 'react';
import { connect } from 'react-redux';
import { searchQuery, searchNoResult } from '../../actions/searchActions';
import store from "../../store"

@connect((store) => {
    return {
        search: store.search.query,
        entries: store.entry.entries
    }
})

export default class SearchBar extends React.Component  {

    constructor(props) {
        super(props)

        this.handleInputChange = this.handleInputChange.bind(this)
    }

    handleInputChange(query) {

        let searchResults = []

        // Filter store.entry.entries and based on user input ( store.search.query )
        searchResults = this.props.entries.filter(function (entry) {

            query = query.toLowerCase()
            let title = '', body = '', url = '', tags = ''

            if (entry.fields.title) title = entry.fields.title.toLowerCase()
            if (entry.fields.url) url = entry.fields.url.toLowerCase()
            if (entry.fields.body) body = entry.fields.body.toLowerCase()
            if (entry.fields.tags) tags = entry.fields.tags

            // return if there is a match
            return title.indexOf(query) != -1 || body.indexOf(query) != -1 || url.indexOf(query) != -1 || tags.indexOf(query) != -1
        });

        // dispatch searchResults array
        return this.props.dispatch(searchQuery(query, searchResults))
    }

    render() {
        const {search, value} = this.props;

        return (
            <div className="search">
                <input className="search-bar" onChange={(e) => this.handleInputChange(e.target.value)} value={value} />
            </div>
        )
    }
}
