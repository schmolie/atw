import React from 'react'
import { TiTimes } from  'react-icons/lib/ti'


export default class Notification extends React.Component  {

    constructor(props) {
        super()

    }

    render() {

        return (
            <div className={ 'notification' }>
                <strong>{ this.props.notification }</strong>
                <button className="notification--dismiss icon-close" value="dismiss" onClick={ this.props.toggleNotification }>
                    <TiTimes/>
                </button>
            </div>
        )
    }
}
