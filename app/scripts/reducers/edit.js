export default function reducer(state = {
    active: false,
    entry: null,
    updatedEntry: null,
    contentType: null,
    error: false
}, action) {
    switch(action.type) {
        case "EDIT_ENTRY_ID": {
            return {
                ...state,
                active: action.payload
            }
        }
        case "EDIT_ENTRY_CONTENT_TYPE": {
            return {
                ...state,
                contentType: action.payload
            }
        }
        case "EDIT_ENTRY_PENDING": {
            return {
                ...state,
                entry: action.payload
            }
        }
        case "EDIT_ENTRY_FULFILLED": {
            return {
                ...state,
                active: false,
                updatedEntry: action.payload
            }
        }
        case "EDIT_ENTRY_REJECTED": {
            return {
                ...state,
                active: false,
                entry: null,
                error: action.payload
            }
        }
    }
    return state
}
