export default function reducer(state = {
    id: null
    entry: null,
}, action) {
    switch(action.type) {
        case "DELETE_ENTRY_PENDING": {
            return {
                ...state,
                id: action.payload
            }
        }
        case "DELETE_ENTRY": {
            return {
                ...state,
                entry: action.payload
            }
        }
    }
    return state
}
