export default function reducer(state = {
    active: false,
    entry: null,
    error: false
}, action) {
    switch(action.type) {
        case "CREATE_ENTRY_PENDING": {
            return {
                ...state,
                active: action.payload
            }
        }
        case "CREATE_ENTRY_FULFILLED": {
            return {
                ...state,
                active: false,
                entry: action.payload
            }
        }
        case "CREATE_ENTRY_REJECTED": {
            return {
                ...state,
                active: false,
                error: action.payload
            }
        }
    }
    return state
}
