export default function reducer(state = {
    query: null,
    entries: []
}, action) {
    switch(action.type) {
        case "SEARCH_QUERY": {
            return {
                ...state,
                query: action.payload,
            }
        }
        case "SEARCH_RESULT": {
            return {
                ...state,
                entries: action.payload,
            }
        }
    }
    return state
}
