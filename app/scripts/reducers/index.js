import { combineReducers } from 'redux'

import entry from './entry'
import search from './search'
import notification from './notification'
import create from './create'
import edit from './edit'

export default combineReducers({
    search,
    entry,
    create,
    edit,
    notification
})
