export default function reducer(state = {
    active: false,
    message: false
}, action) {
    switch(action.type) {
        case "NOTIFICATION": {
            return {
                ...state,
                active: true,
                message: action.payload
            }
        }
        case "DISMISS_NOTIFICATION": {
            return {
                ...state,
                message: false,
                active: action.payload
            }
        }
    }
    return state
}
